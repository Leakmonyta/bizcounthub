import React, { Component } from 'react';

class CustomerAssistance extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="page-container pt-40 pt-10">
          <div className="container">
            <section className="section faq-area pb-60">
              <h3 className="h-title mb-30 t-uppercase">Frequently Asked Questions</h3>
              <div id="accordion" className="panel-group">
                <h4 className="mb-15">Payments</h4>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h5 className="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#faq_payment0">Nunc ut erat at massa elementum tempus ?</a>
                    </h5>
                  </div>
                  <div id="faq_payment0" className="panel-collapse collapse in">
                    <div className="panel-body">
                      <p class='mb-30'>Integer aliquam sed ante non volutpat. Aenean vitae nulla varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim. Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl elementum, luctus velit ut, viverra tellus.</p>
                      <p>Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu rhoncus nisi viverra. Curabitur sit amet erat nulla.</p>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h5 className="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" className="collapsed" href="#faq_payment1">Nunc ut erat at massa elementum tempus ?</a>
                    </h5>
                  </div>
                  <div id="faq_payment1" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p class='mb-30'>Integer aliquam sed ante non volutpat. Aenean vitae nulla varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim. Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl elementum, luctus velit ut, viverra tellus.</p>
                      <p>Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu rhoncus nisi viverra. Curabitur sit amet erat nulla.</p>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h5 className="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" className="collapsed" href="#faq_payment2">Nunc ut erat at massa elementum tempus ?</a>
                    </h5>
                  </div>
                  <div id="faq_payment2" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p class='mb-30'>Integer aliquam sed ante non volutpat. Aenean vitae nulla varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim. Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl elementum, luctus velit ut, viverra tellus.</p>
                      <p>Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu rhoncus nisi viverra. Curabitur sit amet erat nulla.</p>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h5 className="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" className="collapsed" href="#faq_payment3">Nunc ut erat at massa elementum tempus ?</a>
                    </h5>
                  </div>
                  <div id="faq_payment3" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p class='mb-30'>Integer aliquam sed ante non volutpat. Aenean vitae nulla varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim. Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl elementum, luctus velit ut, viverra tellus.</p>
                      <p>Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu rhoncus nisi viverra. Curabitur sit amet erat nulla.</p>
                    </div>
                  </div>
                </div>
                <h3 className="mb-20 mt-40">How we work</h3>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h5 className="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" className="collapsed" href="#faq_work0">Nunc ut erat at massa elementum tempus ?</a>
                    </h5>
                  </div>
                  <div id="faq_work0" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p class='mb-30'>Integer aliquam sed ante non volutpat. Aenean vitae nulla varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim. Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl elementum, luctus velit ut, viverra tellus.</p>
                      <p>Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu rhoncus nisi viverra. Curabitur sit amet erat nulla.</p>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h5 className="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" className="collapsed" href="#faq_work1">Nunc ut erat at massa elementum tempus ?</a>
                    </h5>
                  </div>
                  <div id="faq_work1" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p class='mb-30'>Integer aliquam sed ante non volutpat. Aenean vitae nulla varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim. Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl elementum, luctus velit ut, viverra tellus.</p>
                      <p>Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu rhoncus nisi viverra. Curabitur sit amet erat nulla.</p>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h5 className="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" className="collapsed" href="#faq_work2">Nunc ut erat at massa elementum tempus ?</a>
                    </h5>
                  </div>
                  <div id="faq_work2" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p class='mb-30'>Integer aliquam sed ante non volutpat. Aenean vitae nulla varius, dictum nisi non, rhoncus sem. Vivamus vel velit semper, sagittis ante vel, tempor augue. Proin quis justo auctor, auctor risus vitae, tempor enim. Aliquam erat volutpat. Phasellus facilisis aliquam eleifend. Donec eget nisl elementum, luctus velit ut, viverra tellus.</p>
                      <p>Aenean id aliquam velit, eget consequat neque. Suspendisse potenti. Praesent id cursus odio, eget aliquet lectus. Pellentesque id commodo diam. Aliquam in urna tincidunt, ullamcorper sapien at, imperdiet ex. Mauris laoreet pellentesque mi quis ornare. Donec non pulvinar nulla. Aenean suscipit tellus ut ex luctus, eu rhoncus nisi viverra. Curabitur sit amet erat nulla.</p>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default CustomerAssistance;